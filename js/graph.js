class Graph {
  constructor(canvas) {
    this.ctx = canvas.getContext("2d");
  }

  get edges() {
    return this._edges;
  }

  get vertexes() {
    return this._vertexes;
  }

  /* VERTEXES */
  createVertex(x, y) {
    const currentVertex = this.findCurrentVertex(x, y);

    if (!currentVertex) {
      this._vertexes.push(new Vertex(x, y, this._vertexes.length));
      return true;
    }

    return false;
  }

  moveVertex(vertex, { x, y }) {
    vertex.x = x;
    vertex.y = y;

    this.redraw();
  }

  /* VERTEXES */

  /* EDGES */
  createEdge(vertex1, vertex2, weight) {
    this._edges.push(new Edge(vertex1, vertex2, weight));
  }

  edgeVertexesConnected(vertex1, vertex2) {
    return this._edges.find(edge =>
      (edge.vertex1 === vertex1 && edge.vertex2 === vertex2) || (edge.vertex2 === vertex1 && edge.vertex1 === vertex2)
    );
  }

  /* EDGES */

  /* HELPERS */
  findCurrentVertex(x, y) {
    return this._vertexes.find(vertex => vertex.isIn(x, y));
  }

  vertexesNearPoint(x, y, radius = 100) {
    return this._vertexes.filter(vertex => Math.sqrt(Math.pow(vertex.x - x, 2) + Math.pow(vertex.y - y, 2)) < radius);
  }

  vertexesNearVertex(vertex1, radius = 100) {
    return this._vertexes.filter(vertex2 => vertex1 !== vertex2 && Math.sqrt(Math.pow(vertex1.x - vertex2.x, 2) + Math.pow(vertex1.y - vertex2.y, 2)) < radius);
  }

  dijkstra(sourceVertex, targetVertex, highlightRoutes = false) {
    const vectors = new Set();
    const prev = {};
    const dist = {};
    const adj = {};
    const visited = { vertexes: new Set(), edges: new Set() };

    const source = sourceVertex.index.toString();
    const target = targetVertex.index.toString();

    // Init variables
    for (let i = 0; i < this._edges.length; i++) {
      let { vertex1: { index: v1 }, vertex2: { index: v2 }, weight } = this._edges[i];

      v1 = v1.toString();
      v2 = v2.toString();

      vectors.add(v1);
      vectors.add(v2);

      dist[v1] = Infinity;
      dist[v2] = Infinity;

      if (adj[v1] === undefined) adj[v1] = {};
      if (adj[v2] === undefined) adj[v2] = {};

      adj[v1][v2] = weight;
      adj[v2][v1] = weight;
    }

    /// Function for compare distance
    const vertexWithMinDist = (vectors, dist) => {
      let min = Infinity;
      let vectorWithMin = null;

      for (let vector of vectors) {
        if (dist[vector] < min) {
          min = dist[vector];
          vectorWithMin = vector;
        }
      }

      return vectorWithMin
    };

    dist[source] = 0;

    // Main
    while (vectors.size) {
      let vector = vertexWithMinDist(vectors, dist);
      if (!vector) return [[], Infinity];
      let neighbors = Object.keys(adj[vector]).filter(v => vectors.has(v));  // Neighbors still exist

      vectors.delete(vector);

      if (vector === target) break;                                          // Break when the target has been found

      for (let v of neighbors) {
        let alt = dist[vector] + adj[vector][v];
        if (alt < dist[v]) {
          dist[v] = alt;
          prev[v] = vector;

          if (highlightRoutes) {
            const vertex1 = this.vertexes[Number(v)];
            const vertex2 = this.vertexes[Number(vector)];
            const edge = this.edgeVertexesConnected(vertex1, vertex2);

            visited.vertexes.add(vertex1);
            visited.vertexes.add(vertex2);
            visited.edges.add(edge);
          }
        }
      }
    }

    // End
    let u = target;
    let path = [u];
    let len = 0;

    while (prev[u] !== undefined) {
      path.unshift(prev[u]);
      len += adj[u][prev[u]];
      u = prev[u];
    }

    return [path, len, visited];
  }

  AStar(sourceVertex, targetVertex, highlightRoutes = false) {
    const self = this;
    const sourceNode = new Node(sourceVertex.index);
    const map = initMap();
    const visited = { vertexes: new Set(), edges: new Set() };

    const openset = new Set();
    const closedset = new Set();

    openset.add(sourceNode);

    while (openset.size) {
      let x = minFNode(openset);

      if (x.name === targetVertex.index) return reconstructPath(sourceNode, x);

      openset.delete(x);
      closedset.add(x);

      let neighborsNames = Array.from(map.get(x.name).keys()).filter(vector => !closedset.has(vector));    // Neighbors which not in closed set

      for (let neighborName of neighborsNames) {
        let gScore = x.g + map.get(x.name).get(neighborName);

        let findedNeighbor = Array.from(openset.values()).find(node => node.name === neighborName);
        if (!findedNeighbor) {
          const neighbor = new Node(neighborName, x, gScore);
          openset.add(neighbor);

          if (highlightRoutes) {
            const vertex1 = this.vertexes[Number(neighborName)];
            const vertex2 = this.vertexes[Number(x.name)];
            const edge = this.edgeVertexesConnected(vertex1, vertex2);

            visited.vertexes.add(vertex1);
            visited.vertexes.add(vertex2);
            visited.edges.add(edge);
          }
        } else {
          if (gScore < findedNeighbor.g) {
            findedNeighbor.parent = x;
            findedNeighbor.g = gScore;
            findedNeighbor.f = findedNeighbor.g + findedNeighbor.h;
          }
        }
      }
    }

    return [0, [], { edges: new Set(), vertexes: new Set() }];

    function Node(index, parent = null, g = 0) {
      this.name = index;
      this.parent = parent;

      this.g = g || 0;
      this.h = heuristic(self.vertexes[index], targetVertex);
      this.f = this.g + this.h;
    }

    function heuristic(vertex1, vertex2) {
      const dx2 = Math.pow(vertex2.x - vertex1.x, 2);
      const dy2 = Math.pow(vertex2.y - vertex1.y, 2);
      return Math.sqrt(dx2 + dy2);
    }

    function reconstructPath(startNode, endNode) {
      const pathMap = [];
      let current = endNode;

      while (current !== null) {
        pathMap.unshift(current.name);
        current = current.parent;
      }

      return [pathMap, endNode.g, visited];
    }

    function minFNode(nodes) {
      let result = undefined;
      let minF = Infinity;

      nodes.forEach(node => {
        if (node.f < minF) {
          minF = node.f;
          result = node;
        }
      });

      return result;
    }

    function initMap() {
      const map = new Map();

      for (let i = 0; i < self._edges.length; i++) {
        let { vertex1: { index: v1 }, vertex2: { index: v2 }, weight } = self._edges[i];

        if (!map.has(v1)) map.set(v1, new Map());
        if (!map.has(v2)) map.set(v2, new Map());

        map.get(v1).set(v2, weight);
        map.get(v2).set(v1, weight);
      }

      return map;
    }
  }

  /* HELPERS */

  /* CORE */

  highlight(edges = [], vertexes = [], style) {
    Edge.initHighlightStyle(this.ctx, style);
    edges.forEach(edge => {
      edge.draw(this.ctx);
    });

    Vertex.initHighlightStyle(this.ctx, style);
    vertexes.forEach(vertex => {
      vertex.draw(this.ctx);
    });
  }

  redraw() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

    Vertex.initStyle(this.ctx);
    this._vertexes.forEach(vertex => {
      vertex.draw(this.ctx);
    });

    Edge.initStyle(this.ctx);
    this._edges.forEach(edge => {
      edge.draw(this.ctx);
    });
  }

  init() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this._edges = [];
    this._vertexes = [];

    for (let i = 0; i < 1000; i++) {
      let x = 0;
      let y = 0;

      do {
        x = (Math.random() * (canvas.width - 10)) + 5;
        y = (Math.random() * (canvas.height - 10)) + 5;
      } while (!this.createVertex(x, y));
    }

    this.redraw();

    for (let i = 0; i < 1000; i++) {
      const vertex = this.vertexes[i];
      const near = this.vertexesNearVertex(vertex);

      let second = -1;
      for (let j = 0; j < 2; j++) {
        let newSecond = -2;

        do {
          newSecond = Math.floor(Math.random() * (near.length - 1));
        } while(newSecond === second);

        second = newSecond;
        const randomVertex = near[second];
        this.createEdge(vertex, randomVertex);
      }
    }

    this.redraw();
  }

  /* CORE */
}

class Vertex {
  get index() {
    return this._index;
  }

  static radius() {
    return 3
  };

  static initStyle(ctx) {
    ctx.strokeStyle = "#000";
    ctx.lineJoin = "round";
    ctx.lineWidth = 3;
    ctx.font = 'bold 14px serif';
  }

  static initHighlightStyle(ctx, { stroke, width, font }) {
    ctx.strokeStyle = stroke || "#FFF";
    ctx.lineJoin = "round";
    ctx.lineWidth = width || 3;
    ctx.font = font || 'bold 14px serif';
  }

  constructor(x, y, index) {
    this._x = x;
    this._y = y;
    this._index = index;
  }

  draw(ctx) {
    ctx.beginPath();
    ctx.arc(this._x, this._y, Vertex.radius(), 0, 2 * Math.PI, false);
    ctx.closePath();
    ctx.stroke();
  }

  distanceTo(x, y) {
    const distX = Math.abs(x - this._x);
    const distY = Math.abs(y - this._y);
    return Math.sqrt(distX * distX + distY * distY);
  }

  isIn(x, y) {
    return this.distanceTo(x, y) < (Vertex.radius() * 3);
  }

  get y() {
    return this._y;
  }

  set y(value) {
    this._y = value;
  }

  get x() {
    return this._x;
  }

  set x(value) {
    this._x = value;
  }
}

class Edge {
  static initStyle(ctx) {
    ctx.strokeStyle = "#404040";
    ctx.lineJoin = "round";
    ctx.lineWidth = 2;
    ctx.font = '10px serif';
  }

  static initHighlightStyle(ctx, { stroke, width, font }) {
    ctx.strokeStyle = stroke || "#F22121";
    ctx.lineJoin = "round";
    ctx.lineWidth = width || 2;
    ctx.font = font || '10px serif';
  }

  constructor(vertex1, vertex2) {
    this._weight = Math.floor(Math.sqrt(Math.pow(vertex1.x - vertex2.x, 2) + Math.pow(vertex1.y - vertex2.y, 2)));
    this._vertex1 = vertex1;
    this._vertex2 = vertex2;
  }

  draw(ctx) {
    ctx.beginPath();
    ctx.moveTo(this.vertex1.x, this.vertex1.y);
    ctx.lineTo(this.vertex2.x, this.vertex2.y);
    ctx.closePath();
    ctx.stroke();

    const textX = (this.vertex1.x + this.vertex2.x) / 2;
    const textY = (this.vertex1.y + this.vertex2.y) / 2;
    ctx.fillText(this._weight, textX + 10, textY);
  }

  get vertex1() {
    return this._vertex1;
  }

  set vertex1(value) {
    this._vertex1 = value;
  }

  get vertex2() {
    return this._vertex2;
  }

  set vertex2(value) {
    this._vertex2 = value;
  }

  get weight() {
    return this._weight;
  }

  set weight(value) {
    this._weight = Number(value) ? value : 0;
  }
}