const canvas = document.querySelector(".canvas");
const menu_button = document.querySelector('.button_menu');

canvas.width = canvas.offsetWidth;
canvas.height = canvas.offsetHeight;

let graph = new Graph(canvas);
let movingState = new MovingState();
let pathfindingState = new PathfindingState();

/* CANVAS */
canvas.addEventListener('mousedown', function (e) {
  closeMenu();

  const mouseX = e.layerX;
  const mouseY = e.layerY;
  const vertexFound = graph.findCurrentVertex(mouseX, mouseY);

  if (vertexFound) {
    movingState = new MovingState(vertexFound);

    if (pathfindingState.mode === 4) {
      if (!pathfindingState.vertex1) {
        pathfindingState.vertex1 = vertexFound;
      } else if (pathfindingState.isEdit1) {
        pathfindingState.vertex1 = vertexFound;
        pathfindingState.isEdit1 = false;
      } else if (!pathfindingState.isEdit1) {
        pathfindingState.vertex2 = vertexFound;
        pathfindingState.isEdit1 = true;
        pathfindingState.run();
      }
    }
  }
});

canvas.addEventListener('mousemove', function (e) {
  if (movingState.isMove) {
    const x = e.layerX;
    const y = e.layerY;

    graph.moveVertex(movingState.vertex, { x, y });
  }
});

canvas.addEventListener('mouseup', () => {
  movingState = new MovingState();
});

canvas.addEventListener('mouseleave', () => {
  movingState = new MovingState();
});

function getPathElements(path) {
  const vertexIndexes = path.map(Number);

  const vertexesHightlight = graph.vertexes.filter(vertex => vertexIndexes.includes(vertex.index));
  const edgesHighlight = graph.edges.filter(edge => {
    const vertex1Index = vertexIndexes.indexOf(edge.vertex1.index);
    const vertex2Index = vertexIndexes.indexOf(edge.vertex2.index);

    if (vertex1Index < 0 || vertex2Index < 0) return false;
    else return vertex1Index === vertex2Index + 1 || vertex1Index === vertex2Index - 1;
  });

  return [edgesHighlight, vertexesHightlight];
}

/* MODAL */
function menuAction() {
  if (menu_button.dataset.on === "true") {
    openMenu();
  } else {
    closeMenu();
  }
}

function openMenu() {
  document.querySelector('.tools').classList.add('tools--show');
  menu_button.dataset.on = "false";
}

function closeMenu() {
  document.querySelector('.tools').classList.remove('tools--show');
  menu_button.dataset.on = "true";
}

document.querySelector('.button_menu').addEventListener('click', () => menuAction());
document.querySelector('.button_graph').addEventListener('click', () => graph.init());

/* HELPERS */
window.addEventListener('resize', () => {
  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
  graph.redraw();
});

function MovingState(vertex = null) {
  if (!vertex) {
    this.vertex = null;
    this.isMove = false;
  } else {
    this.vertex = vertex;
    this.isMove = true;
  }
}

function PathfindingState() {
  this.mode = 4;
  this.alg = "a";
  this.isEdit1 = false;
  this.highlightPath = true;

  this.loadVertexes = () => {
    switch (this.mode) {
      case 1: {
        const heightCenter = graph.ctx.canvas.height / 2;
        const vertexes1 = graph.vertexesNearPoint(0, heightCenter);
        const vertexes2 = graph.vertexesNearPoint(graph.ctx.canvas.width, heightCenter);

        this.vertex1 = vertexes1[Math.floor(Math.random() * (vertexes1.length - 1))];
        this.vertex2 = vertexes2[Math.floor(Math.random() * (vertexes2.length - 1))];
      }
        break;
      case 2: {
        const side1 = Math.random() > 0.5;
        const side2 = Math.random() > 0.5;

        const point1 = {
          x: side1 ? 0 : graph.ctx.canvas.width,
          y: side2 ? 0 : graph.ctx.canvas.height,
        };
        const point2 = {
          x: side1 ? graph.ctx.canvas.width : 0,
          y: side2 ? graph.ctx.canvas.height : 0,
        };

        const vertexes1 = graph.vertexesNearPoint(point1.x, point1.y);
        const vertexes2 = graph.vertexesNearPoint(point2.x, point2.y);

        this.vertex1 = vertexes1[Math.floor(Math.random() * (vertexes1.length - 1))];
        this.vertex2 = vertexes2[Math.floor(Math.random() * (vertexes2.length - 1))];
      }
        break;
      case 3: {
        const widthCenter = graph.ctx.canvas.width / 2;
        const heightCenter = graph.ctx.canvas.height / 2;
        const vertexes1 = graph.vertexesNearPoint(widthCenter, heightCenter, 50);
        const vertexes2 = graph.vertexesNearPoint(widthCenter, graph.ctx.canvas.height, 50);

        this.vertex1 = vertexes1[Math.floor(Math.random() * (vertexes1.length - 1))];
        this.vertex2 = vertexes2[Math.floor(Math.random() * (vertexes2.length - 1))];
      }
        break;
      default:
        break;
    }
  };

  this.run = () => {
    if (graph.vertexes) {
      graph.redraw();

      let start = 0;
      let end = 0;
      switch (this.alg) {
        case "a":
          start = performance.now();
          a(this.vertex1, this.vertex2, this.highlightPath);
          end = performance.now();
          break;
        case "d":
          start = performance.now();
          d(this.vertex1, this.vertex2, this.highlightPath);
          end = performance.now();
          break;
        default:
          break;
      }

      document.querySelector(".tools__ms").textContent = `${(end - start).toFixed(2)} ms`
    }
  }
}

document.querySelectorAll('.radio_algorythm_input').forEach(input =>
  input.addEventListener('change', (e) => {
    pathfindingState.alg = e.target.value;
    pathfindingState.run();
  })
);

document.querySelectorAll('.radio_mode_input').forEach(input =>
  input.addEventListener('click', (e) => {
    const mode = Number(e.target.value);

    pathfindingState.mode = mode;
    if (mode !== 4) {
      pathfindingState.loadVertexes();
      pathfindingState.run();
    }
  })
);

function a(vertex1, vertex2, highlight = true) {
  console.info("A*");
  console.time("A* time");
  const [pathA, lenA, visitedA] = graph.AStar(vertex1, vertex2, highlight);
  console.timeEnd("A* time");
  console.log(pathA);
  console.log("\n");

  const { edges: edgesVisited, vertexes: vertexesVisited } = visitedA;
  graph.highlight(edgesVisited, vertexesVisited, { stroke: "#F22121", width: 3 });

  const [edgesA, vertexesA] = getPathElements(pathA);
  graph.highlight(edgesA, vertexesA, { stroke: "#2196F3", width: 5 });
}

function d(vertex1, vertex2, highlight = true) {
  console.info("D");
  console.time("D time");
  const [pathD, lenD, visitedD] = graph.dijkstra(vertex1, vertex2, highlight);
  console.timeEnd("D time");
  console.log(pathD);
  console.log("\n");

  const { edges: edgesVisited, vertexes: vertexesVisited } = visitedD;
  graph.highlight(edgesVisited, vertexesVisited, { stroke: "#F22121", width: 3 });

  const [edgesD, vertexesD] = getPathElements(pathD);
  graph.highlight(edgesD, vertexesD, { stroke: "#2196F3", width: 5 });
}